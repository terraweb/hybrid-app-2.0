import Vue from 'vue'
export function getEnvironment (envID) {
  return Vue.http.get(`environment/${envID}`)
}

export function getEnvironmentSensorData (envID, startDate, endDate) {
  return Vue.http.get('environment{/id}/sensors/between', {
    params: {
      id: envID,
      start_date: startDate,
      end_date: endDate
    }
  })
}
