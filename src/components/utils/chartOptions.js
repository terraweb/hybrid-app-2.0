export default {
  credits: {
    enabled: false
  },
  title: {
    text: null,
    x: -20
  },
  colors: [
    '#f44336',
    '#1565c0'
  ],
  chart: {
    type: 'spline',
    zoomType: 'xy',
    panning: true
  },
  xAxis: {
    type: 'datetime',
    title: {
      text: null,
      style: {'color': '#fff'}
    },
    labels: {
      style: {'color': '#fff'}
    }
  },
  yAxis: {
    title: {
      text: null,
      style: {'color': '#fff'}
    },
    labels: {
      style: {'color': '#fff'}
    },
    gridLineColor: 'rgba(255,255,255,0.2)'
  },
  plotOptions: {
    spline: {
      marker: {
        enabled: true
      },
      dataLabels: {
        enabled: true,
        useHTML: true,
        style: {
          color: 'white'
        }
      }
    }
  },
  tooltip: {
    followPointer: false,
    followTouchMove: false
  },
  series: []
}
