// === DEFAULT / CUSTOM STYLE ===
// WARNING! always comment out ONE of the two require() calls below.
// 1. use next line to activate CUSTOM STYLE (./src/themes)
require(`./themes/app.${__THEME}.styl`)
// 2. or, use next line to activate DEFAULT QUASAR STYLE
// require(`quasar/dist/quasar.${__THEME}.css`)
// ==============================

import Vue from 'vue'
import Quasar from 'quasar'
import router from './router'
import VueResource from 'vue-resource'

Vue.router = router

Vue.use(VueResource)
Vue.http.options.root = 'http://api.terrariums.eu/api/v1'

Vue.use(Quasar) // Install Quasar Framework

Vue.use(require('@websanova/vue-auth'), {
  auth: require('@websanova/vue-auth/drivers/auth/bearer.js'),
  http: require('@websanova/vue-auth/drivers/http/vue-resource.1.x.js'),
  router: require('@websanova/vue-auth/drivers/router/vue-router.2.x.js'),
  logoutData: {
    redirect: '/login'
  }
})

Quasar.start(() => {
  /* eslint-disable no-new */
  new Vue({
    el: '#q-app',
    router,
    created () {
      Vue.http.interceptors.push(function (request, next) {
        next((response) => {
          if (response.status === 400 && (['token_invalid', 'token_not_provided'].indexOf(response.body.error) > -1)) {
            console.log('Logging out')
            this.$auth.logout()
          }
        })
      })
    },
    render: h => h(require('./App'))
  })
})
